import 'package:flutter/material.dart';
import 'gamehomeui.dart';
import 'package:get/get.dart';

void main() => runApp(MyMinesWeeper());

class MyMinesWeeper extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'MinesWeeper Game',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: GameHome(),
    );
  }
}