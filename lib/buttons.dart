import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {

  final color;
  final textColor;
  final String buttonText;
  final buttOnTapped;
  final index;
  final Widget widget;
  final indexTouch;

//Constructor
  MyButton({this.color, this.textColor, this.buttonText, this.buttOnTapped, this.index, this.widget, this.indexTouch});

  @override
  Widget build(BuildContext context) {
    if(buttonText == ''){
      return GestureDetector(
        onTap: buttOnTapped,
        child: Padding(
          padding: const EdgeInsets.all(0.4),
          child: ClipRRect(
            //borderRadius: BorderRadius.circular(2),
            child: Container(
              color: color,
              child: Center(
                child: Image.asset(
                  'images/mine.png',
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
        ),
      );
    }else{
      return GestureDetector(
        onTap: buttOnTapped,
        child: Padding(
          padding: const EdgeInsets.all(0.4),
          child: ClipRRect(
            //borderRadius: BorderRadius.circular(2),
            child: Container(
              color: color,
              child: Center(
                child: Text(
                  buttonText,
                  style: TextStyle(
                    color: textColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }
  }
}
