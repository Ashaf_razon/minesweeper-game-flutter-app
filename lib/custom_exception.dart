import 'package:flutter/material.dart';

class MyCustException implements Exception{
  static String expMsgTimerNotStop() => 'Timer could not stop by the system, check it Urgently!!!';
  static String expMsgCallTimer() => 'Timer could not stop by the system, check it Urgently!!!';

  static String expMsgMineInit() => 'Mine Initializing Error, please check the mines';
  static String expMsgMineBoardInit() => 'Mine Board Initializing Error, please check the board';
  static String expMsgMineBoardView() => 'Mine Board View Error, please check the print board';
}