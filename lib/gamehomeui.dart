import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:minesweeper/mines_weeper_grid_board.dart';
//import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'count_up_timer.dart';
import 'mines_weeper_algo.dart';

class GameHome extends StatelessWidget {
  int themeDataUser = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.redAccent,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'images/mine.png',
              fit: BoxFit.contain,
              height: 40,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              child: Text('MINESWEEPER'),
            ),
            Image.asset(
              'images/minesweeperman.png',
              fit: BoxFit.contain,
              height: 40,
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text('Mines Hunter Leader'),
              accountEmail: Text('hunter@mine.weeper'),
              arrowColor: Colors.redAccent,
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.redAccent,
                child: Image.asset('images/minesweeperman.png',
                    fit: BoxFit.contain),
              ),
            ),
            ListTile(
              leading: Icon(Icons.score_sharp),
              title: Text('View Best Scores'),
              onTap: () {
                Get.defaultDialog(middleText: 'Best score is: 0',
                    title: 'Sorry, No best score has done yet!!!');
              },
            ),
            ListTile(
              leading: Icon(Icons.invert_colors_rounded),
              title: Text('Change Game Theme Color'),
              onTap: () {
                themeDataUser += 1;
                if (themeDataUser % 2 == 1)
                  Get.changeTheme(ThemeData.dark());
                else
                  Get.changeTheme(ThemeData.fallback());
                //Navigator.pop(context);
                Get.back();
              },
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: GameBodyBuilder(),
    );
  }
}

class GameBodyBuilder extends StatefulWidget {
  const GameBodyBuilder({Key key}) : super(key: key);

  @override
  _GameBodyBuilderState createState() => _GameBodyBuilderState();
}

class _GameBodyBuilderState extends State<GameBodyBuilder> {
  int userGameFlag = 2;
  final int _lose = 0;
  final int _win = 1;
  final int _busy = 2;
  int timerValue;
  int boardLen;
  int gridLen;
  List mineBoard;
  //minesAlgo.minesViewBoard();
  final CounterController controller = Get.put(CounterController());
  MinesWeeperAlgo minesAlgo;

  void _minesWeeperSetBoard() {
    setState(() {
      minesAlgo = MinesWeeperAlgo();
      minesAlgo.minesWeeperMinesGenerator();
      mineBoard = minesAlgo.minesWeeperMineEdgeGenerator();
      /*
      Timer(Duration(seconds: 1), () {
        //minesAlgo.minesViewBoard();
        minesAlgo = null;
      });*/
    });
  }

  @override
  Widget build(BuildContext context) {
    minesAlgo = MinesWeeperAlgo();
    boardLen = minesAlgo.nn;
    gridLen = boardLen * boardLen;
    minesAlgo.minesWeeperMinesGenerator();
    //mineBoard = minesAlgo.minesWeeperMineEdgeGenerator();
    /*
    Timer(Duration(seconds: 2), () {
      minesAlgo.minesViewBoard();
    });*/

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.all(10.0),
            color: Colors.green,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    child: Image.asset(
                      'images/game.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    child: Card(
                      child: Image.asset(
                        'images/busy.png',
                        fit: BoxFit.contain,
                      ),
                      elevation: 15,
                      shape: CircleBorder(),
                      shadowColor: Colors.amber,
                      color: Colors.white24,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Center(
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      child: Obx(
                        () => Text(
                          '${controller.countTime}',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 4,
          child: Container(
            padding: EdgeInsets.all(2.0),
            color: Colors.red,
            child: MinesWeeperGridBoard(
              gridLen: gridLen,
              boardLen: boardLen,
              //mineBoard: mineBoard,
              minesAlgo: minesAlgo,
            )
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
              color: Colors.red,
              child: Center(
                child: Container(
                  padding: EdgeInsets.all(15.0),
                  color: Colors.red,
                  child: MaterialButton(
                      //height: 20.0,
                      color: Colors.white,
                      child: Text(
                        'Game Set/Reset',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      textColor: Colors.red,
                      splashColor: Colors.grey,
                      onPressed: () {
                        // Start Timer
                        controller.callTimer();
                        _minesWeeperSetBoard();
                      }),
                ),
              )),
        ),
      ],
    );
  }
}
