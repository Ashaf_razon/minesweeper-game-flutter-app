import 'dart:collection';
import 'custom_exception.dart';

class MinesWeeperSolution{

  List findAdjacencyZero(var source, List minesBoard){
    List locOfAdjZero = [[]];
    Queue zeroBFS = new Queue();
    int r, c;
    final nn = 10;// as it is a 10 grid board
    final visited = 50; //if node already visited

    if(minesBoard[source[0]][source[1]] == 0){
      minesBoard[source[0]][source[1]] = visited;
      zeroBFS.add(source);
    }

    try{
      while(zeroBFS.isNotEmpty){
        var indexZero = zeroBFS.first;
        r = indexZero[0];
        c = indexZero[1];

        // if position rs not a minesBoardB
        if ((r > 0) && minesBoard[r - 1][c] == 0) {
          zeroBFS.addLast([r-1,c]);
          minesBoard[r - 1][c] = visited;
        }
        if ((r > 0) && (c < nn - 1) && minesBoard[r - 1][c + 1] == 0) {
          zeroBFS.addLast([r-1,c+1]);
          minesBoard[r - 1][c + 1] = visited;

        }
        if ((c < nn - 1) && minesBoard[r][c + 1] == 0) {
          zeroBFS.addLast([r,c+1]);
          minesBoard[r][c + 1] = visited;
        }
        if ((r < nn - 1) && (c < nn - 1) && minesBoard[r + 1][c + 1] == 0) {
          zeroBFS.addLast([r+1,c+1]);
          minesBoard[r + 1][c + 1] = visited;
        }
        if ((r < nn - 1) && minesBoard[r + 1][c] == 0) {
          zeroBFS.addLast([r+1,c]);
          minesBoard[r + 1][c] = visited;
        }
        if ((r < nn - 1) && (c > 0) && minesBoard[r + 1][c - 1] == 0) {
          //if Empty is in the any middle position
          zeroBFS.addLast([r+1,c-1]);
          minesBoard[r + 1][c - 1] = visited;
        }
        if ((c > 0) && minesBoard[r][c - 1] == 0) {
          // if Empty is in the any middle position
          zeroBFS.addLast([r,c-1]);
          minesBoard[r][c - 1] = visited;
        }
        if ((r > 0) && (c > 0) && minesBoard[r - 1][c - 1] == 0) {
          // if Empty is in the any middle position
          zeroBFS.addLast([r-1,c-1]);
          minesBoard[r - 1][c - 1] = visited;
        }
        //Zero BFS print
        print(zeroBFS);

        //location print
        locOfAdjZero.add(indexZero);
        zeroBFS.removeFirst();
      }
      minesSolZeroViewBoard(minesBoard, nn);// view the new board with zero discover
    }catch(e){
      print(e);
    }
    return minesBoard; /// new [MinesBoard] replace ZERO adjacent with 50
    /// new [locOfAdjZero] contains all adjacent ZERO location of grid minesBoard
  }

  void minesSolZeroViewBoard(List minesBoard, int nn) {
    try{
      int i;
      print('MinesSolution --> Reinit Board');
      for (i = 0; i < nn; i++) {
        print(minesBoard[i]);
      }
    }catch(E){
      print(MyCustException.expMsgMineBoardView()+' Exception at Mines Solution = '+E.toString());
    }
  }
}