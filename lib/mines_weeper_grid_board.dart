import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'MinesWeeperSolution.dart';
import 'buttons.dart';
import 'mines_weeper_algo.dart';

class MinesWeeperGridBoard extends StatefulWidget {
  final int boardLen;
  final int gridLen;
  final MinesWeeperAlgo minesAlgo;
  List mineBoard;
  MinesWeeperGridBoard({Key key, this.boardLen, this.gridLen, this.minesAlgo}) : super(key: key){
    mineBoard = minesAlgo.minesWeeperMineEdgeGenerator();
  }

  @override
  _MinesWeeperGridBoardState createState() => _MinesWeeperGridBoardState();
}

class _MinesWeeperGridBoardState extends State<MinesWeeperGridBoard> {
  List indexTo2D = [0, 0];
  int countTapFlag = 100;
  //MinesWeeperAlgo minesAlgo = Get.put(MinesWeeperAlgo());
  MinesWeeperSolution minesSolution = Get.put(MinesWeeperSolution());

  @override
  Widget build(BuildContext context) {
    print('Initial value');
    print(widget.gridLen);
    print(widget.boardLen);
    print(widget.mineBoard);
    return Container(
      child: GridView.builder(
          itemCount: widget.gridLen,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: widget.boardLen),
          itemBuilder: (BuildContext context, int index) {
            indexTo2D = widget.minesAlgo.indexTo2Dposition(index);
            //print(indexTo2D.toString());
            return MyButton(
              //index: indexTo2D,// always finds the last position, need to comment out
              buttOnTapped: () {
                indexTo2D = widget.minesAlgo.indexTo2Dposition(index); //Get the Tapped button index
                print("Helloo Zero "+indexTo2D.toString());
                setState(() {
                  widget.mineBoard = minesSolution.findAdjacencyZero(indexTo2D, widget.mineBoard);
                  countTapFlag = widget.mineBoard[indexTo2D[0]][indexTo2D[1]];
                });
              },
              buttonText: buttonUiViewDesign(widget.mineBoard[indexTo2D[0]][indexTo2D[1]]),
              ///if button text = 20 then replace with mine.png image
              ///else button text = tex as it was
              color: Colors.grey, ///all grid button color
               ///if text is 0 then change the color to pink
              textColor: widget.mineBoard[indexTo2D[0]][indexTo2D[1]] == 50? Colors.pink: Colors.black,
              //buttonUiViewColor(countTapFlag),
            );
          }), // GridView.builder
    );
  }

  String buttonUiViewDesign(mineBoard) {
    if(widget.mineBoard[indexTo2D[0]][indexTo2D[1]] == 20){
      return '';
    }else if(widget.mineBoard[indexTo2D[0]][indexTo2D[1]] == 50){
      return '0';
    }else{
      return widget.mineBoard[indexTo2D[0]][indexTo2D[1]].toString();
    }
  }

  Color buttonUiViewColor(int countTapFlag) {
    //red is just a sample color
    Color color;
    if(countTapFlag == 100){
      color = Colors.grey;
      print("Grey 1: "+countTapFlag.toString());
    }else if(countTapFlag == 0){
      countTapFlag = 100;
      print("Pink: "+countTapFlag.toString());
      color = Colors.pink;
    }else if(countTapFlag>=1 && countTapFlag<=9){
      color = Colors.black;
      countTapFlag = 100;
      print("Viol: "+countTapFlag.toString());
    }else{
      color = Colors.grey;
      countTapFlag = 100;
      print("Others: "+countTapFlag.toString());
    }
    return color;
  }
}
