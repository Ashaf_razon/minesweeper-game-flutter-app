import 'dart:math';
import 'package:get/get.dart';
import 'custom_exception.dart';

class MinesWeeperAlgo extends GetxController{
  var nn = 10;
  var i, j;
  bool isMineGenerated = false;
  var bom = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
  ];

  void minesWeeperMinesGenerator() {
    try{
      //minesViewBoard();
      if(isMineGenerated == false){
        print('1 --> Mines');
        Random random = new Random();
        for (i = 0; i < nn; i++) {
          int randomNumber = random.nextInt(nn); // from 0 upto 10 included
          bom[i][randomNumber] = 20;
        }
        isMineGenerated = true;
      }
    }catch(E){
      print(MyCustException.expMsgMineInit()+' Exception= '+E.toString());
    }
  }

  List minesWeeperMineEdgeGenerator() {
    if (isMineGenerated) {
      //minesViewBoard();
      try{
        print('2 --> Board');
        for (i = 0; i < nn; i++) {
          for (j = 0; j < nn; j++) {
            if (bom[i][j] != 20) {
              // if position is not a BOMB
              if ((i > 0) && bom[i - 1][j] == 20) {
                bom[i][j] += 1;
              }
              if ((i > 0) && (j < nn - 1) && bom[i - 1][j + 1] == 20) {
                bom[i][j] += 1;
              }
              if ((j < nn - 1) && bom[i][j + 1] == 20) {
                bom[i][j] += 1;
              }
              if ((i < nn - 1) && (j < nn - 1) && bom[i + 1][j + 1] == 20) {
                bom[i][j] += 1;
              }
              if ((i < nn - 1) && bom[i + 1][j] == 20) {
                bom[i][j] += 1;
              }
              if ((i < nn - 1) && (j > 0) && bom[i + 1][j - 1] == 20) {
                // if Empty is in the any middle position
                bom[i][j] += 1;
              }
              if ((j > 0) && bom[i][j - 1] == 20) {
                // if Empty is in the any middle position
                bom[i][j] += 1;
              }
              if ((i > 0) && (j > 0) && bom[i - 1][j - 1] == 20) {
                // if Empty is in the any middle position
                bom[i][j] += 1;
              }
            }
          }
        }
        isMineGenerated = false;
      }catch(E){
        print(MyCustException.expMsgMineBoardInit()+' Exception= '+E.toString());
      }
    }
    return bom;
  }

  void minesViewBoard() {
    try{
      print('3 --> Reinit Board');
      for (i = 0; i < nn; i++) {
        print(bom[i]);
      }
    }catch(E){
      print(MyCustException.expMsgMineBoardView()+' Exception= '+E.toString());
    }
  }

  List indexTo2Dposition(int index){
    List indexTo2D = [10,20];
    int r = index ~/ 10;
    int c = index % 10;
    indexTo2D[0] = r;
    indexTo2D[1] = c;
    return indexTo2D;
  }
}
