import 'package:get/get.dart';
import 'dart:async';
import 'custom_exception.dart';

class CounterController extends GetxController {
  var countTime = 0.obs;
  String timerShow = '00:00';
  Timer globTimer;
  int counterFlagForTime = 0;

  Timer interval(Duration duration, func) {
    Timer function() {
      Timer timer = new Timer(duration, function);
      func(timer);
      return timer;
    }
    return new Timer(duration, function);
  }
  callTimer(){
    try{
      if(counterFlagForTime %2 == 0){
        interval(new Duration(seconds: 1), (timer) {
          countTime++;
          //double minute = countTime / 30;
          //int second = countTime % 30;
          //timerShow = minute.toString()+' : '+second.toString();
          globTimer = timer;
          if (countTime > 499){
            try{
              timer.cancel();
            }catch(E){
              print(MyCustException.expMsgTimerNotStop()+'Interval Count Exception= '+E.toString());
            }
          }
        });
      }else{
        try{
          globTimer.cancel();
        }catch(E){
          print(MyCustException.expMsgTimerNotStop()+'Interval Count Exception= '+E.toString());
        }
      }
      counterFlagForTime++;
    }catch(E){
      print(MyCustException.expMsgCallTimer()+'Interval Exception= '+E.toString());
      Get.defaultDialog(title: MyCustException.expMsgCallTimer());
    }
  }
  @override
  void dispose() async {
    try{
      globTimer.cancel();
    }catch(E){
      print(MyCustException.expMsgTimerNotStop()+'Dispose Exception= '+E.toString());
      Get.defaultDialog(title: 'Sorry, System could not stop the timer');
    }
    super.dispose(); // Need to call dispose function.
  }
}